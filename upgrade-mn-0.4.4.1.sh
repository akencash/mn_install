#!/bin/bash
# Compatible with Ubuntu 16.04

set -o errexit

sudo systemctl stop akencash
sudo wget https://bitbucket.org/akencash/akencash/downloads/akencash-0.4.4.1-cli-linux.tar.gz --directory-prefix /home/akencash/ -O /home/akencash/akencash-0.4.4.1-cli-linux.tar.gz
sudo tar -xzvf /home/akencash/akencash-0.4.4.1-cli-linux.tar.gz -C /home/akencash/
sudo chown -R akencash:akencash /home/akencash/akencash* && sudo chmod 755 /home/akencash/akencash*
sudo rm /home/akencash/akencash-0.4.4.1-cli-linux.tar.gz
sudo systemctl start akencash

echo "Masternode upgrade complete!"
