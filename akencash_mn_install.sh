#!/bin/bash
#
# Copyright (C) 2018 AKN Team <dev@aken.cash>
#
# mn_install.sh is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# mn_install.sh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with mn_install.sh. If not, see <http://www.gnu.org/licenses/>
#

# Only Ubuntu 16.04 supported at this moment.

set -o errexit

# OS_VERSION_ID=`gawk -F= '/^VERSION_ID/{print $2}' /etc/os-release | tr -d '"'`

sudo apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade
sudo apt install curl wget git python3 python3-pip python-virtualenv -y

AKN_DAEMON_USER_PASS=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c 24 ; echo ""`
AKN_DAEMON_RPC_PASS=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c 24 ; echo ""`
MN_NAME_PREFIX=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c 6 ; echo ""`
MN_EXTERNAL_IP=`curl -s -4 ifconfig.co`

sudo useradd -U -m akencash -s /bin/bash
echo "akencash:${AKN_DAEMON_USER_PASS}" | sudo chpasswd
sudo wget https://bitbucket.org/akencash/akencash/downloads/akencash-0.4.4.1-cli-linux.tar.gz --directory-prefix /home/akencash/
sudo tar -xzvf /home/akencash/akencash-0.4.4.1-cli-linux.tar.gz -C /home/akencash/
sudo rm /home/akencash/akencash-0.4.4.1-cli-linux.tar.gz
sudo mkdir /home/akencash/.akencashcore/
sudo chown -R akencash:akencash /home/akencash/akencash*
sudo chmod 755 /home/akencash/akencash*
echo -e "rpcuser=akenrpc\nrpcpassword=${AKN_DAEMON_RPC_PASS}\nlisten=1\nserver=1\nrpcallowip=127.0.0.1\nmaxconnections=256" | sudo tee /home/akencash/.akencashcore/akencash.conf
sudo chown -R akencash:akencash /home/akencash/.akencashcore/
sudo chown 500 /home/akencash/.akencashcore/akencash.conf

sudo tee /etc/systemd/system/akencash.service <<EOF
[Unit]
Description=Akencash, distributed currency daemon
After=network.target

[Service]
User=akencash
Group=akencash
WorkingDirectory=/home/akencash/
ExecStart=/home/akencash/akencashd

Restart=always
PrivateTmp=true
TimeoutStopSec=60s
TimeoutStartSec=2s
StartLimitInterval=120s
StartLimitBurst=5

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl enable akencash
sudo systemctl start akencash
echo "Booting AKN node and creating keypool"
sleep 140

MNGENKEY=`sudo -H -u akencash /home/akencash/akencash-cli masternode genkey`
echo -e "masternode=1\nmasternodeprivkey=${MNGENKEY}\nexternalip=${MN_EXTERNAL_IP}:14933" | sudo tee -a /home/akencash/.akencashcore/akencash.conf
sudo systemctl restart akencash

echo "Installing sentinel engine"
sudo git clone https://bitbucket.org/akencash/sentinel.git /home/akencash/sentinel/
sudo chown -R akencash:akencash /home/akencash/sentinel/
cd /home/akencash/sentinel/
sudo -H -u akencash virtualenv -p python3 ./venv
sudo -H -u akencash ./venv/bin/pip install -r requirements.txt
echo "* * * * * akencash cd /home/akencash/sentinel && ./venv/bin/python bin/sentinel.py >/dev/null 2>&1" | sudo tee /etc/cron.d/akencash_sentinel
sudo chmod 644 /etc/cron.d/akencash_sentinel

echo " "
echo " "
echo "==============================="
echo "Masternode installed!"
echo "==============================="
echo "Copy and keep that information in secret:"
echo "Masternode key: ${MNGENKEY}"
echo "SSH password for user \"akencash\": ${AKN_DAEMON_USER_PASS}"
echo "Prepared masternode.conf string:"
echo "mn_${MN_NAME_PREFIX} ${MN_EXTERNAL_IP}:14933 ${MNGENKEY} INPUTTX INPUTINDEX"

exit 0
